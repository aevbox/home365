const SERVER = 'localhost';
const PORT = '3000';
const url = `${SERVER}:${PORT}`

export const BASE_URL = `http://${url}/properties`;

export enum EStatus {
  active = 'active',
  occupied = 'occupied',
  vacant = 'vacant',
  inactive = 'inactive'
}

export enum ETenantStatus  {
  all = 'all',
  active = 'active',
  inactive = 'inactive'
}

export interface IResponse {
  "propertyId": string,
  "createdOn": Date,
  "address": string,
  "occupiedStats": EStatus,
  "owner": string,
  "ownerStatus": EStatus,
  "tenant": {
    "contactId": string,
    "firstName": string,
    "lastName": string,
    "tenantStatus": EStatus
  },
  "plan": string
}

export interface IFetchOptions {
  id?: string;
  occupiedStats?: EStatus;
  tenantStatus?: ETenantStatus;
  page?: number;
  limit?: number;
}

