import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {BASE_URL, EStatus, ETenantStatus, IFetchOptions, IResponse} from './api.models';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ApiServiceService {

  constructor(private http: HttpClient) {
  }

  getAll(): Observable<IResponse[]> {
    return this.http.get<IResponse[]>(BASE_URL);
  }

  getById(id: string): Observable<IResponse | undefined> {
    const options = {id};
    return this.http.get<IResponse[]>(BASE_URL, {params: this.queryBuilder(options)})
      .pipe(map(response => {
        return response?.length ? response[0] : undefined;
      }));
  }

  getByConditions(options: IFetchOptions) {
    return this.http.get<IResponse[]>(BASE_URL, {
      observe: 'response',
      params: this.queryBuilder(options),
    });
  }

  private queryBuilder(
    options: IFetchOptions,
  ): HttpParams {
    let result = new HttpParams()

    if (options.id) {
      result = result.append('propertyId', options.id);
    }

    if (options.occupiedStats) {
      if (options.occupiedStats === EStatus.active) {
        result = result.append('occupiedStats_ne', EStatus.inactive)
      } else {
        result = result.append('occupiedStats', options.occupiedStats)
      }
    }

    if (options.tenantStatus && options.tenantStatus != ETenantStatus.all) {
      result = result.append('tenant.tenantStatus', options.tenantStatus)
    }

    if (options.page != undefined) {
      result = result.append('_page', options.page);
    }

    if (options.limit != undefined) {
      result = result.append('_limit', options.limit);
    }

    return result
  }
}
