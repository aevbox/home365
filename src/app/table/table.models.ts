import {EStatus} from '../services/api.models';

export const COLUMNS = ['Created', 'Property', 'Property Status', 'Plan', 'Owner', 'Owner Status', 'Tenant', 'Tenant Status'];

export enum EDialogTypes {
  address,
  owner,
  tenant
}

export interface IDialogData {
  title: string;
  data?: {
    text: string,
    textStatus: string,
    status: EStatus
  }
}
