import {Component, EventEmitter, Input, Output} from '@angular/core';
import {PageEvent} from '@angular/material/paginator';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss'],
})
export class PaginatorComponent {
  pageSize = 10;
  pageIndex = 0;
  pageSizeOptions = [5, 10, 25];
  showFirstLastButtons = true;
  @Input() length = 0;
  @Output() pageEvent = new EventEmitter<PageEvent>()

  handlePageEvent(event: PageEvent) {
    this.pageEvent.emit(event);
  }
}
