import {Component} from '@angular/core';
import {EStatus, ETenantStatus, IFetchOptions, IResponse} from '../services/api.models';
import {ApiServiceService} from '../services/api-service.service';
import {PageEvent} from '@angular/material/paginator';
import {MatDialog} from '@angular/material/dialog';
import {DialogComponent} from './dialog/dialog.component';
import {COLUMNS, EDialogTypes, IDialogData} from './table.models';
import {BehaviorSubject} from 'rxjs';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent {
  dialogTypes = EDialogTypes;
  displayedColumns: string[] = COLUMNS;

  response: IResponse[] | undefined | null;
  allCount = 0;

  private options = {
    occupiedStats: EStatus.active,
    tenantStatus: ETenantStatus.all,
    page: 1,
    limit: this.allCount,
  };

  private changedOptions$ = new BehaviorSubject(this.options);

  constructor(private apiService: ApiServiceService, public dialog: MatDialog) {
    this.changedOptions$.subscribe(options => {
      this.options = options;
      this.fetchData(options)
    });
  }

  fetchData(options: IFetchOptions) {
    this.apiService.getByConditions(options).subscribe(response => {
      this.allCount = Number(response.headers.get('X-Total-Count'));
      this.response = response?.body;
    })
  }

  changePage(event: PageEvent) {
    this.changedOptions$.next({...this.options, page: ++event.pageIndex, limit: event.pageSize});
  }

  changeProperty(event: EStatus) {
    this.changedOptions$.next({...this.options, occupiedStats: event});
  }

  changeTenant(event: ETenantStatus) {
    this.changedOptions$.next({...this.options, tenantStatus: event});
  }

  openDialog(event: IResponse, type: EDialogTypes) {
    this.dialog.open(DialogComponent, {
      width: '250px',
      data: this.optionsBuilder(event, type),
    });
  }

  private optionsBuilder(event: IResponse, type: EDialogTypes): IDialogData | undefined {
    let options: IDialogData | undefined;

    if (type === EDialogTypes.address) {
      options = {
        title: 'Property',
        data: {
          text: event.address,
          textStatus: EStatus[event.occupiedStats],
          status: event.occupiedStats,
        },
      }
    }

    if (type === EDialogTypes.owner) {
      options = {
        title: 'Owner',
        data: {
          text: event.owner,
          textStatus: EStatus[event.ownerStatus],
          status: event.ownerStatus,
        },
      }
    }

    if (type === EDialogTypes.tenant) {
      options = {
        title: 'Tenant',
        data: {
          text: `${event.tenant.firstName} ${event.tenant.lastName}`,
          textStatus: EStatus[event.tenant.tenantStatus],
          status: event.tenant.tenantStatus,
        },
      }
    }
    return options;
  }
}

