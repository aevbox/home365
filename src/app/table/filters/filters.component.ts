import {Component, EventEmitter, Output} from '@angular/core';
import {EStatus, ETenantStatus} from '../../services/api.models';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss'],
})
export class FiltersComponent {
  status = Object.entries(EStatus);
  tenantStatus = Object.entries(ETenantStatus);

  @Output() changeProperty = new EventEmitter<EStatus>();
  @Output() changeTenant = new EventEmitter<ETenantStatus>();
}
