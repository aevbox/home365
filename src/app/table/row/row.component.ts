import {Component, EventEmitter, Input, Output} from '@angular/core';
import {EStatus, IResponse} from '../../services/api.models';

@Component({
  selector: '[app-row]',
  templateUrl: './row.component.html',
  styleUrls: ['./row.component.scss'],
})
export class RowComponent {
  status = EStatus;

  @Input('data') data: IResponse | undefined
  @Output('propertyClick') propertyEvent = new EventEmitter<IResponse>();
  @Output('ownerClick') ownerEvent = new EventEmitter<IResponse>();
  @Output('tenantClick') tenantEvent = new EventEmitter<IResponse>();

  propertyClick() {
    this.propertyEvent.emit(this.data);
  }

  ownerClick() {
    this.ownerEvent.emit(this.data);
  }

  tenantClick() {
    this.tenantEvent.emit(this.data);
  }
}
