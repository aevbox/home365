import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TableComponent} from './table.component';
import {RowComponent} from './row/row.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatFormFieldModule} from '@angular/material/form-field';
import {PaginatorComponent} from './paginator/paginator.component';
import {DialogComponent} from './dialog/dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import {MatSelectModule} from '@angular/material/select';
import {FiltersComponent} from './filters/filters.component';


@NgModule({
  declarations: [
    TableComponent,
    RowComponent,
    PaginatorComponent,
    DialogComponent,
    FiltersComponent,
  ],
  imports: [
    CommonModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatDialogModule,
    MatButtonModule,
    MatSelectModule,
  ],
  exports: [TableComponent],
})
export class TableModule {
}
